[Home](../README.md)


# Table of Contents
- [Pipeline](#pipeline)
- [Deployment](#deployment)

---

## Pipeline

![pipeline](images/pipeline-trigger-2.png)

- Type은 helm chart로 release까지만 할 것인지, 아니면 deploy가지 해야 하는지 설정
- Deploy 설정 시  target namespace도 설정 해야함.

### pipeline 구성
![pipeline tasks](images/pipeline-tasks.png)

- Predefined된 pipeline 사용
- 조직에 일괄적인 pipeline 적용으로 표준화 할 수 있음
- 개발쪽에서 추가적인 configuration 없이 deployment target에 맞게 ISW가 설정함
- 추가 custom pipeline 사용 가능

## Deployment
![solutionhub](images/solution%20hub%20position.png)
![solutionenvoy](images/solution%20envoy%20position.png)

- solution hub -> solution Envoy 접근하여 deployment 확인
https://education-dev.apps.openshift-01.knowis.cloud/solutions.html