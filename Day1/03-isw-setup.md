[Home](../README.md)

# Table of Contents

ISW 설정
1. [ISW 사용 준비하기](#isw-사용-준비하기)
2. [ISW 사용하기](#isw-사용하기)


# ISW 설정

## ISW 사용 준비하기

### Step 1. GitLab 토큰 등록하기
ISW가 본인의 GitLab과 연동될 수 있도록 합니다. 

1. GitLab에 로그인한 후 오른쪽 상단 프로필 아이콘을 클릭합니다.
    - 이 때, 나의 계정 이름을 기억해둡니다. *(ex. @s.ryu)*
2. **Edit Profile**을 선택하여 계정 설정 화면에 진입합니다.
3. 왼쪽 Navigation 메뉴에서 **Access Tokens**를 클릭합니다.
4. 필요한 값들을 입력합니다. 
    - Token name: isw
    - Expiration date: 임의의 날짜
    - Select scopes: **모든 scope 선택**
5. Create personal access token 버튼을 클릭합니다.
6. 화면에 나타난 토큰을 복사합니다. 
    - 이 토큰은 다시 볼 수 없으므로 *메모장* 등에 복사하여두세요. 


### Step 2. ISW에 토큰 등록하기
1. ISW의 메인 인터페이스인 [솔루션 디자이너](https://edu-k5-designer.apps.openshift-01.knowis.cloud/) 콘솔에 접속합니다. 
안내 받은 계정으로 로그인 합니다. 
2. 오른쪽 상단 프로필 아이콘을 클릭합니다.
3. **User Settings**를 선택하여 계정 설정 화면에 진입합니다.  
4. **Git Tokens** 탭을 선택합니다.
5. **Create git token**을 선택하고 필요한 값들을 입력합니다.
    - Token Name: gitlab
    - Git Provider: gitlab.com
    - Git Username: GitLab의 유저 이름 *(ex. @s.ryu 에서 s.ryu 부분. Case-sensitive)*
    - Git Access Token: Step 1에서 생성한 Personal access token
6. Verify and Create 버튼을 눌러 토큰을 등록합니다.



## ISW 사용하기

### Step 1. Workspace 생성하기

솔루션 디자이너의 메인 화면에서 나의 워크스페이스를 확인할 수 있습니다. 워크스페이스는 ISW 프로젝트의 조합입니다. 이번 실습에서는 미리 생성된 **IBEE** 프로젝트들을 사용합니다. 

1. 워크스페이스 화면에서 **Create workspace** 버튼을 클릭합니다.
2. Workspace Name: 적당한 이름을 입력합니다.
3. Workspace projects: *All tags* 필터를 누르고 **IBEE**를 선택하여 검색된 프로젝트 중 `Position Keeping`과 `Servicing Order`를 제외한 5개의 프로젝트를 선택합니다. 
    - *Internal Bank Account*
    - *Party Lifecycle Management*
    - *Payment Execution*
    - *Payment Order*
    - *Savings Account*


### Step 2. ISW CLI 사용하기

Solution Desinger에서 UI로 보이는 프로젝트를 소스 코드의 형태로 clone 받기 위해서 CLI를 사용합니다. 

1. ISW의 새로 생성한 워크스페이스에서 임의의 프로젝트를 선택합니다.
2. 프로젝트 화면에서 브라우저의 맨 아래 **Solution CLI** 버튼을 클릭하세요. 
3. 최상단 **Solution CLI Setup** 카드를 선택하세요.
4. `cli-config.json` 파일을 다운로드합니다.
5. `cli-config.json` 파일의 내용을 복사하여 Gitpod에서 새 파일을 만들고 내용을 붙여넣기 합니다.

 ***cli-config.json 파일을 레포지토리에 push하지 마세요!***

6. Gitpod에서 ISW CLI(`k5`)를 사용할 수 있는지 확인합니다. 

현재 디렉토리에 설치된 ISW CLI(`k5`)를 실행하기 위해서 아래 명령어를 수행하세요. **`k5` CLI를 사용할 때 항상 `npx` 명령어를 앞에 추가하여야합니다.**

```
npx k5
```

> 원래는 ISW의 Solution CLI Setup 가이드에 따라 `npm install --global file:CLI.tar` 명령어를 수행해야하지만 `/workspace` 이외 영역의 변경 사항이 저장이 되지 않는 Gitpod의 특성으로 인하여 현재 디렉토리에 CLI를 설치해두었습니다.

10. 아래 명령어를 수행하여 CLI로 로그인을 진행합니다.
```
npx k5 setup --file cli-config.json
```
이 후 Gitpod 인스턴스가 종료 후 재실행될 때마다 로그인을 새로 진행하여 주세요. 
